<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Lazy_Jerry_Initialization
 * @subpackage Lazy_Jerry_Initialization/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Lazy_Jerry_Initialization
 * @subpackage Lazy_Jerry_Initialization/includes
 * @author     Your Name <email@example.com>
 */
class Lazy_Jerry_Initialization_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}

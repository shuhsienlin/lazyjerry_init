<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Lazy_Jerry_Initialization
 * @subpackage Lazy_Jerry_Initialization/admin/partials
 * 
 * ref: http://getbootstrap.com/components/
 */
// var_dump($_POST);die();
$options = get_option('lazy_jerry_initialization_public_options');
$defaultOpts = array(
			"enableGa"			=> false,
			"gaID" 				=> "", 
			"disableFileEdit" 	=> true,
		);

if( isset($options) && !empty($options) && is_array($options)){
	$options = array_merge($defaultOpts, $options);
}else{
	$options = $defaultOpts;
}

if( get_current_user_id() == $_POST['postVal'] ){
	if( isset($_POST['enableGa']) && !empty($_POST['enableGa']) ){
		$options["enableGa"] = true;
	}else{
		$options["enableGa"] = false;
	}

	if( isset($_POST['gaID']) && !empty($_POST['gaID']) ){
		$options["gaID"] = $_POST['gaID'];
	}else{
		$options["gaID"] = "";
	}

	if( isset($_POST['disableFileEdit']) && $_POST['disableFileEdit'] == "0" ){
		$options["disableFileEdit"] = true;
	}else{
		$options["disableFileEdit"] = false;
	}

	if( update_option('lazy_jerry_initialization_public_options', $options, true)){
		echo "<script>alert('儲存成功！')</script>";
	}else{
		echo "<script>alert('資料未更動！')</script>";
	}
}


$isReadMeFileExist = file_exists(ABSPATH."readme.html");
$delReadMeFileState = -1;
if($isReadMeFileExist && isset($_POST['delFile']) && $_POST['delFile'] == "1"){
	$delReadMeFileState = (unlink(ABSPATH."readme.html"))?1:0;
}else if(!$isReadMeFileExist){
	$delReadMeFileState = -1;
}

$disableFileEdit_dropdown_text_def = "後台編輯器";
$disableFileEdit_dropdown_text_dis = "停用";
$disableFileEdit_dropdown_text_act = "啟用";


?>
<div class="container">
	<h1>Wordpress 懶人包設定</h1>
	<h2>初始化安全設定、建議安裝外掛、提供一次性安全與優化調整。</h2>
	<ul class="nav nav-tabs">
	    <li class="active"><a data-toggle="tab" href="#intro">介紹</a></li>
	    <li><a data-toggle="tab" href="#setting">設定</a></li>
	    <li><a data-toggle="tab" href="#check">檢查</a></li>
	    <li><a data-toggle="tab" href="#plugins">推薦外掛</a></li>
	  </ul>
	<div class="tab-content">
		<div class="tab-pane fade in active" id="intro">
			<div class="col-lg-6">
				<p class="lead">
					<h3>設定的功能</h3>
					<span>1.停用後台編輯器</span><br/>
					<span>2. GA PageView 設定</span><br/>
					<span>3. 刪除根目錄底下readme.html</span><br/>
					<span>4. 建議安裝外掛</span><br/>
				</p>
			</div>
			<div class="col-lg-6">
				<p class="lead">
					<h3>執行的功能</h3>
					<span>1. 移除 style 和 script 的版本號碼</span><br/>
					<span>2. 移除不必要的鏈結（版本號碼）</span><br/>
					<span>3. 停用 emoji_styles</span><br/>
					<span>4. 停用迴響的 HTML 功能</span><br/>
					<span>5. 隱藏 feed</span><br/>
					<span>6. 隱藏真正的登入錯誤訊息</span><br/>
					<span>7. 停用網址猜測功能</span><br/>
				</p>
			</div>
		</div> <!-- tab-pane -->
		<div class="tab-pane fade" id="setting">
			<h3>設定選項</h3>
			<div class="col-lg-12">
				<form method="POST" action="" onsubmit="return formSubmit();">
					<label for="ga_id">GA 追蹤編號設定（勾選表示啟用）</label>
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon">
						        <input type="checkbox" aria-label="..." id="enableGa" name="enableGa" onclick="checkEnableGa();" <?php echo ($options["enableGa"])?"checked":"" ?> >
						    </span>
						  	<span class="input-group-addon" id="basic-addon3">UA-</span>
						  	<input type="text" class="form-control" id="ga_id" name="gaID" aria-describedby="basic-addon3" aria-label="..." value="<?php echo $options["gaID"]; ?>">
						</div>
					</div>
					<div class="form-group">
						<div class="input-group">
							<input type="hidden" id="disableFileEdit" name="disableFileEdit" value="<?php echo ($options['disableFileEdit'])?'0':'1'; ?>" />
							<div class="dropdown" id="dropbox_area">
								<label for="dropbox_area" style="margin-right: 20px;"><?php echo $disableFileEdit_dropdown_text_def; ?></label>
								<button class="btn btn-default dropdown-toggle" type="button" id="disableFileEdit_dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
									<?php echo $disableFileEdit_dropdown_text_def; ?>
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu" aria-labelledby="disableFileEdit_dropdown">
									<li><a href="#" onclick="selectDisableFileEdit(0);return false;"><?php echo $disableFileEdit_dropdown_text_dis; ?></a></li>
									<li><a href="#" onclick="selectDisableFileEdit(1);return false;"><?php echo $disableFileEdit_dropdown_text_act; ?></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="form-group">
						<input type="hidden" name="postVal" value="<?php echo get_current_user_id(); ?>">
						<button type="submit" id="submit" class="btn btn-default hidden" style="float: right;">儲存全部</button>
					</div>
				</form>
			</div>
		</div> <!-- tab-pane -->
		<div class="tab-pane fade" id="check">
			<h3>安全檢查</h3>
			<div class="col-lg-12">	
				<label for="del_file">刪除Wrodpress Readme.html</label>
				<div class="form-group">
					<div class="input-group">
						<button type="button" id="del_file" class="btn btn-default" <?php echo ($isReadMeFileExist)?"":"disabled" ?> ><?php echo ($isReadMeFileExist)?"點我刪除":"(已刪除)" ?></button>
					</div>
				</div>
			</div>
		</div> <!-- tab-pane -->
		<div class="tab-pane fade" id="plugins">
			<h3>推薦外掛</h3>
			<div class="col-lg-12">	
				<label>建議安裝外掛：</label>
				<div class="form-group">
					<img width="30" src="//ps.w.org/w3-total-cache/assets/icon-256x256.png?rev=1041806" alt="">
					<a href="/wp-admin/plugin-install.php?tab=plugin-information&amp;plugin=w3-total-cache&amp;TB_iframe=true&amp;width=772&amp;height=451" class="" target="_blank">
						W3 Total Cache
					</a>
				</div>
				<div class="form-group">
					<img width="30" src="//ps.w.org/wordpress-seo/assets/icon.svg?rev=1266422" alt="">
					<a href="/wp-admin/plugin-install.php?tab=plugin-information&amp;plugin=wordpress-seo&amp;TB_iframe=true&amp;width=600&amp;height=550" class="" target="_blank">
						Yoast SEO
					</a>
				</div>
				<div class="form-group">
					<img  width="30" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI0MzMiIGhlaWdodD0iNDMzIiB2aWV3Ym94PSIwIDAgNDMzIDQzMyIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgZmlsbD0icmdiKDUzLCAxMjEsIDE1MykiIC8+PHJlY3QgeD0iMCIgeT0iNiIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMjAiIG9wYWNpdHk9IjAuMTUiIGZpbGw9IiMyMjIiIC8+PHJlY3QgeD0iMCIgeT0iMzEiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjUiIG9wYWNpdHk9IjAuMDIiIGZpbGw9IiNkZGQiIC8+PHJlY3QgeD0iMCIgeT0iNDQiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjE4IiBvcGFjaXR5PSIwLjEzMjY2NjY2NjY2NjY3IiBmaWxsPSIjMjIyIiAvPjxyZWN0IHg9IjAiIHk9IjcxIiB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxNCIgb3BhY2l0eT0iMC4wOTgiIGZpbGw9IiMyMjIiIC8+PHJlY3QgeD0iMCIgeT0iOTAiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjExIiBvcGFjaXR5PSIwLjA3MiIgZmlsbD0iI2RkZCIgLz48cmVjdCB4PSIwIiB5PSIxMDkiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjE4IiBvcGFjaXR5PSIwLjEzMjY2NjY2NjY2NjY3IiBmaWxsPSIjMjIyIiAvPjxyZWN0IHg9IjAiIHk9IjE0NiIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTkiIG9wYWNpdHk9IjAuMTQxMzMzMzMzMzMzMzMiIGZpbGw9IiNkZGQiIC8+PHJlY3QgeD0iMCIgeT0iMTc3IiB3aWR0aD0iMTAwJSIgaGVpZ2h0PSI3IiBvcGFjaXR5PSIwLjAzNzMzMzMzMzMzMzMzMyIgZmlsbD0iI2RkZCIgLz48cmVjdCB4PSIwIiB5PSIyMDAiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjExIiBvcGFjaXR5PSIwLjA3MiIgZmlsbD0iI2RkZCIgLz48cmVjdCB4PSIwIiB5PSIyMTciIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjIwIiBvcGFjaXR5PSIwLjE1IiBmaWxsPSIjMjIyIiAvPjxyZWN0IHg9IjAiIHk9IjI0OSIgd2lkdGg9IjEwMCUiIGhlaWdodD0iOSIgb3BhY2l0eT0iMC4wNTQ2NjY2NjY2NjY2NjciIGZpbGw9IiNkZGQiIC8+PHJlY3QgeD0iMCIgeT0iMjY3IiB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxMCIgb3BhY2l0eT0iMC4wNjMzMzMzMzMzMzMzMzMiIGZpbGw9IiMyMjIiIC8+PHJlY3QgeD0iMCIgeT0iMjgzIiB3aWR0aD0iMTAwJSIgaGVpZ2h0PSI1IiBvcGFjaXR5PSIwLjAyIiBmaWxsPSIjZGRkIiAvPjxyZWN0IHg9IjAiIHk9IjMwMCIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTMiIG9wYWNpdHk9IjAuMDg5MzMzMzMzMzMzMzMzIiBmaWxsPSIjZGRkIiAvPjxyZWN0IHg9IjAiIHk9IjMyMyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTkiIG9wYWNpdHk9IjAuMTQxMzMzMzMzMzMzMzMiIGZpbGw9IiNkZGQiIC8+PHJlY3QgeD0iMCIgeT0iMzQ4IiB3aWR0aD0iMTAwJSIgaGVpZ2h0PSI2IiBvcGFjaXR5PSIwLjAyODY2NjY2NjY2NjY2NyIgZmlsbD0iIzIyMiIgLz48cmVjdCB4PSIwIiB5PSIzNjIiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjE4IiBvcGFjaXR5PSIwLjEzMjY2NjY2NjY2NjY3IiBmaWxsPSIjMjIyIiAvPjxyZWN0IHg9IjAiIHk9IjM4NiIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTUiIG9wYWNpdHk9IjAuMTA2NjY2NjY2NjY2NjciIGZpbGw9IiNkZGQiIC8+PHJlY3QgeD0iMCIgeT0iNDE0IiB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxOSIgb3BhY2l0eT0iMC4xNDEzMzMzMzMzMzMzMyIgZmlsbD0iI2RkZCIgLz48cmVjdCB4PSI2IiB5PSIwIiB3aWR0aD0iMjAiIGhlaWdodD0iMTAwJSIgb3BhY2l0eT0iMC4xNSIgZmlsbD0iIzIyMiIgLz48cmVjdCB4PSIzMSIgeT0iMCIgd2lkdGg9IjUiIGhlaWdodD0iMTAwJSIgb3BhY2l0eT0iMC4wMiIgZmlsbD0iI2RkZCIgLz48cmVjdCB4PSI0NCIgeT0iMCIgd2lkdGg9IjE4IiBoZWlnaHQ9IjEwMCUiIG9wYWNpdHk9IjAuMTMyNjY2NjY2NjY2NjciIGZpbGw9IiMyMjIiIC8+PHJlY3QgeD0iNzEiIHk9IjAiIHdpZHRoPSIxNCIgaGVpZ2h0PSIxMDAlIiBvcGFjaXR5PSIwLjA5OCIgZmlsbD0iIzIyMiIgLz48cmVjdCB4PSI5MCIgeT0iMCIgd2lkdGg9IjExIiBoZWlnaHQ9IjEwMCUiIG9wYWNpdHk9IjAuMDcyIiBmaWxsPSIjZGRkIiAvPjxyZWN0IHg9IjEwOSIgeT0iMCIgd2lkdGg9IjE4IiBoZWlnaHQ9IjEwMCUiIG9wYWNpdHk9IjAuMTMyNjY2NjY2NjY2NjciIGZpbGw9IiMyMjIiIC8+PHJlY3QgeD0iMTQ2IiB5PSIwIiB3aWR0aD0iMTkiIGhlaWdodD0iMTAwJSIgb3BhY2l0eT0iMC4xNDEzMzMzMzMzMzMzMyIgZmlsbD0iI2RkZCIgLz48cmVjdCB4PSIxNzciIHk9IjAiIHdpZHRoPSI3IiBoZWlnaHQ9IjEwMCUiIG9wYWNpdHk9IjAuMDM3MzMzMzMzMzMzMzMzIiBmaWxsPSIjZGRkIiAvPjxyZWN0IHg9IjIwMCIgeT0iMCIgd2lkdGg9IjExIiBoZWlnaHQ9IjEwMCUiIG9wYWNpdHk9IjAuMDcyIiBmaWxsPSIjZGRkIiAvPjxyZWN0IHg9IjIxNyIgeT0iMCIgd2lkdGg9IjIwIiBoZWlnaHQ9IjEwMCUiIG9wYWNpdHk9IjAuMTUiIGZpbGw9IiMyMjIiIC8+PHJlY3QgeD0iMjQ5IiB5PSIwIiB3aWR0aD0iOSIgaGVpZ2h0PSIxMDAlIiBvcGFjaXR5PSIwLjA1NDY2NjY2NjY2NjY2NyIgZmlsbD0iI2RkZCIgLz48cmVjdCB4PSIyNjciIHk9IjAiIHdpZHRoPSIxMCIgaGVpZ2h0PSIxMDAlIiBvcGFjaXR5PSIwLjA2MzMzMzMzMzMzMzMzMyIgZmlsbD0iIzIyMiIgLz48cmVjdCB4PSIyODMiIHk9IjAiIHdpZHRoPSI1IiBoZWlnaHQ9IjEwMCUiIG9wYWNpdHk9IjAuMDIiIGZpbGw9IiNkZGQiIC8+PHJlY3QgeD0iMzAwIiB5PSIwIiB3aWR0aD0iMTMiIGhlaWdodD0iMTAwJSIgb3BhY2l0eT0iMC4wODkzMzMzMzMzMzMzMzMiIGZpbGw9IiNkZGQiIC8+PHJlY3QgeD0iMzIzIiB5PSIwIiB3aWR0aD0iMTkiIGhlaWdodD0iMTAwJSIgb3BhY2l0eT0iMC4xNDEzMzMzMzMzMzMzMyIgZmlsbD0iI2RkZCIgLz48cmVjdCB4PSIzNDgiIHk9IjAiIHdpZHRoPSI2IiBoZWlnaHQ9IjEwMCUiIG9wYWNpdHk9IjAuMDI4NjY2NjY2NjY2NjY3IiBmaWxsPSIjMjIyIiAvPjxyZWN0IHg9IjM2MiIgeT0iMCIgd2lkdGg9IjE4IiBoZWlnaHQ9IjEwMCUiIG9wYWNpdHk9IjAuMTMyNjY2NjY2NjY2NjciIGZpbGw9IiMyMjIiIC8+PHJlY3QgeD0iMzg2IiB5PSIwIiB3aWR0aD0iMTUiIGhlaWdodD0iMTAwJSIgb3BhY2l0eT0iMC4xMDY2NjY2NjY2NjY2NyIgZmlsbD0iI2RkZCIgLz48cmVjdCB4PSI0MTQiIHk9IjAiIHdpZHRoPSIxOSIgaGVpZ2h0PSIxMDAlIiBvcGFjaXR5PSIwLjE0MTMzMzMzMzMzMzMzIiBmaWxsPSIjZGRkIiAvPjwvc3ZnPg==" alt="">
					<a href="/wp-admin/plugin-install.php?tab=plugin-information&amp;plugin=automatic-updater&amp;TB_iframe=true&amp;width=772&amp;height=325" class="" target="_blank"> <!-- thickbox -->
						Advanced Automatic Updates
					</a>
				</div>
			</div>
		</div> <!-- tab-pane -->
	</div>  <!-- tab-contant -->
</div><!-- container -->
<script>

function tabCheck(indexName){

}

function checkEnableGa(){
	if(jQuery("#enableGa").prop( 'checked')){
		jQuery("#ga_id").removeAttr('disabled');
	}else{
		jQuery("#ga_id").attr('disabled', "disabled");
	}
}

function selectDisableFileEdit(enableFeature){
	var text = (enableFeature == 0)?"<?php echo $disableFileEdit_dropdown_text_dis; ?>":"<?php echo $disableFileEdit_dropdown_text_act; ?>";
	jQuery('#disableFileEdit_dropdown').html(text);
	jQuery('#disableFileEdit').val(enableFeature);
}

var options = <?php echo $options; ?>;
checkEnableGa();
selectDisableFileEdit(<?php echo ($options['disableFileEdit'])?'0':'1'; ?>);
jQuery('#submit').removeClass('hidden');

function formSubmit(){
	if(jQuery("#enableGa").prop( 'checked')){
		if(jQuery("#ga_id").val() == ""){
			alert("GA啟用時，追蹤編號不能為空！");
			return false;		
		}else if( (jQuery("#ga_id").val().match(/\d+/g) == null) || jQuery("#ga_id").val().match("-") == null ){
			alert("GA的追蹤編號格式錯誤！");
			return false;	
		}
	}
	return true;
}

<?php if($isReadMeFileExist){ ?>
var delFile = function(){
	jQuery('body').append("<form method='POST' action='' id='delFile'> <input type='text' name='delFile' value='1' /></form>");
	jQuery('#delFile').submit();
}
jQuery('#del_file').on('click',function(){ delFile(); });

var delReadMeFileState = <?php echo $delReadMeFileState ?>;
if(delReadMeFileState === 1){
	alert("檔案刪除成功！");
}else if(delReadMeFileState === 0){
	alert("檔案刪除失敗，請檢查檔案權限。");
}
<?php }else{ ?>
jQuery('#del_file').on('click',function(){ alert('已經刪除嘍！');});
<?php }// check file is del ?>
</script>

<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://bitbucket.org/sushinelin/lazyjerry_init/
 * @since             1.0.0
 * @package           Lazy_Jerry_Initialization
 *
 * @wordpress-plugin
 * Plugin Name:       Lazy Jerry - wordpress 懶人包
 * Plugin URI:        https://bitbucket.org/sushinelin/lazyjerry_init/
 * Description:       作為 Wordpress 初始化使用的外掛，檢查並設定安全性機制、安裝建議外掛等等。製作用的框架來自：http://codegen.kickapz.com/
 * Version:           1.0.1
 * Author:            Jerry Lin
 * Author URI:        http://lazyjerry.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       lazy-jerry-initialization
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-lazy-jerry-initialization-activator.php
 */
function activate_plugin_name() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-lazy-jerry-initialization-activator.php';
	Lazy_Jerry_Initialization_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-lazy-jerry-initialization-deactivator.php
 */
function deactivate_plugin_name() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-lazy-jerry-initialization-deactivator.php';
	Lazy_Jerry_Initialization_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_plugin_name' );
register_deactivation_hook( __FILE__, 'deactivate_plugin_name' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-lazy-jerry-initialization.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_plugin_name() {

	$plugin = new Lazy_Jerry_Initialization();
	$plugin->run();

}
run_plugin_name();

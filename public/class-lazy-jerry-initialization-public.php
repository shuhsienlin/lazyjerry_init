<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Lazy_Jerry_Initialization
 * @subpackage Lazy_Jerry_Initialization/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Lazy_Jerry_Initialization
 * @subpackage Lazy_Jerry_Initialization/public
 * @author     Your Name <email@example.com>
 */
class Lazy_Jerry_Initialization_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The options of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $options    The options of this plugin.
	 */
	private $options;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		$this->options 	= get_option('lazy_jerry_initialization_public_options');
		$defaultOpts 	= $this->getDefaultOpts();
		if( isset($this->options) && !empty($this->options) && is_array($this->options)){
			$this->options = array_merge($defaultOpts, $this->options);
		}else{
			$this->options = $defaultOpts;
		}

		//停用迴響的HTML功能(global function)
		add_filter( 'pre_comment_content', 'wp_specialchars' );

		//停用後台編輯器
		define( 'DISALLOW_FILE_EDIT', $this->options['disableFileEdit'] );

		//隱藏feed
		remove_action( 'wp_head', 'feed_links', 2 ); 
		remove_action( 'wp_head', 'feed_links_extra', 3 );
	}

	/**
	 * 獲取預設的 Options
	 *
	 * @since    1.0.0
	 */
	public function getDefaultOpts(){
		$arr = array(
			"enableGa"			=> false,
			"gaID" 				=> "", 
			"disableFileEdit" 	=> true,
		);

		return $arr;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Lazy_Jerry_Initialization_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Lazy_Jerry_Initialization_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/lazy-jerry-initialization-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Lazy_Jerry_Initialization_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Lazy_Jerry_Initialization_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/lazy-jerry-initialization-public.js', array( 'jquery' ), $this->version, false );

	}


	public function stop_guessing($url) {
	 	if (is_404()) {
	 		return false;
	 	}
	 	return $url;
	}



	public function login_error_msg(){

		$msgs = array("幸福的愛情和美滿的家庭不需要媒人—— 佚名",
			"最美好的，也是最痛苦的就是愛情！最大高貴的也是最低賤的就是婚姻和家庭。—— 斯特林堡",
			"男子為了各自家庭而承擔的工作，是努力支撐、發展和維護他們的家；至於女子呢？則是努力維護家庭的秩序，家庭的安適和家庭的可愛。—— 羅斯金",
			"希望好像一個家庭，沒有它，你會覺得生活乏味；有了它，你又覺得天天為它辛勞，是一種煩惱。—— 馬克·吐溫",
			"《大學》之修身、齊家、治國、平天下，基本只是正心、誠意而已。—— 朱熹",
			"一個人的悲劇，往往是個性造成，一個家庭的悲劇，更往往是個性的產物。—— 柏楊",
			"幸福的愛情和美滿的家庭不需要媒—— 俄羅斯",
			"明智的人在勞動中找到自己的幸福，而不在家庭、城市、山區或海市蜃樓裡尋找幸福，浪費時間。誰在絕望中耽於分析自己的內心，以控究自己痛苦的原因和深度，那就宛如把玫瑰枝插枝而操心吧，可不要在傍晚或早晨把它控掘出來，以斷定它是否抽出了幸福的幼芽。而這，我以生命起誓，正是真正的幸福。—— 雷哈尼",
			"勤勞的家庭，飢餓過其門而不入。—— 富蘭克林");

		return $msgs [ array_rand($msgs, 1) ];
	}

	public function add_ga_page_trace(){
		$GA_ID = $this->options['gaID'];

		if(!isset($GA_ID) || empty($GA_ID)){
			return;
		}else{
			// UA-64253855-2
			$GA_ID = "UA-".$GA_ID;
		}

		echo "<script>
	  	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  	ga('create', '{$GA_ID}', 'auto');
	  	ga('send', 'pageview',document.getElementsByTagName('title')[0].innerHTML);

		</script>";
	}

	public function _remove_script_version( $src ){
	    if (strpos($a, $_SERVER['SERVER_NAME'] !== false) ) {
	        $parts = explode( '?', $src );
	        return $parts[0];
	    }
	    return $src;
	}

}

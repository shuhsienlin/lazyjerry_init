<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Lazy_Jerry_Initialization
 * @subpackage Lazy_Jerry_Initialization/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
